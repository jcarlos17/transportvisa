package com.programacionymas.transportvisa.io.response

import com.programacionymas.transportvisa.model.Step

data class DistanceResponse (
    val steps: List<Step>,
    val distance: Int,
    val price: Float,
    val time: String
)