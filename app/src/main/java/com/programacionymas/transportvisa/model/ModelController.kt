package com.programacionymas.transportvisa.model

class ModelController {
    private val travels: ArrayList<Travel> = ArrayList()

    fun addTravel(travel: Travel) {
        travels.add(travel)
    }

    fun getTravelList(): ArrayList<Travel> {
        return travels
    }

    fun getTotal(): String {
        var total = 0f

        travels.forEach {
            total += it.getPriceRounded()
        }

        return "%.2f".format(total)
    }

    fun getListAsString(): String {
        var message = "Transport Visa - Lista de viajes:\n"

        travels.forEach {
            message += "-----------------------------\n"
            message += "Desde: ${it.from}\n"
            message += "Hasta: ${it.to}\n"
            message += "Distancia: ${it.getDistanceInKm()} km\n"
            message += "Tiempo: ${it.time}\n"
            message += "Precio: S/ ${it.getPriceRounded()}\n"
            message += "-----------------------------\n"
        }

        message += "Total: S/ ${getTotal()}"

        return message
    }

    companion object {
        private var instance: ModelController? = null

        fun get(): ModelController {
            if (instance == null) {
                instance = ModelController()
            }

            return instance as ModelController
        }
    }
}