package com.programacionymas.transportvisa.model

/*
{
    "end_location": {
        "lat": -11.9659923,
        "lng": -76.9834426
    },
    "start_location": {
        "lat": -11.9662341,
        "lng": -76.98303349999999
    }
}
*/
data class Step (
        val end_location: Location,
        val start_location: Location
)