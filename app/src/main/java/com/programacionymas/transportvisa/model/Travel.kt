package com.programacionymas.transportvisa.model

data class Travel (
    var from: String = "",
    var to: String = "",

    var distance: Int = -1,
    var price: Float = -1f,
    var time: String = ""
) {

    fun getDistanceInKm(): String {
        val kilometers = distance.toFloat()/1000
        return "%.2f".format(kilometers)
    }

    fun getPriceRoundedAsText(): String {
        return "%.2f".format(price)
    }

    fun getPriceRounded(): Float {
        return getPriceRoundedAsText().toFloat()
    }
}