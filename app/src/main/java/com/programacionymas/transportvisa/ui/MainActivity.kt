package com.programacionymas.transportvisa.ui

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.programacionymas.transportvisa.R
import com.programacionymas.transportvisa.io.ApiService
import com.programacionymas.transportvisa.io.response.DistanceResponse
import com.programacionymas.transportvisa.model.ModelController
import com.programacionymas.transportvisa.model.Step
import com.programacionymas.transportvisa.model.Travel
import com.programacionymas.transportvisa.util.showConfirmDialog
import com.programacionymas.transportvisa.util.toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.layout_travel_info.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity(), OnMapReadyCallback {

    companion object {
        private const val REQUEST_CODE_AUTOCOMPLETE_FROM = 1
        private const val REQUEST_CODE_AUTOCOMPLETE_TO = 2
        private const val TAG = "MainActivity"
    }

    private lateinit var mMap: GoogleMap

    // Origin and Destination
    private var mMarkerFrom: Marker? = null
    private var mMarkerTo: Marker? = null
    private var mFromLatLng: LatLng? = null
    private var mToLatLng: LatLng? = null

    // This is the travel object that is going to be stored in the list after being validated
    private val currentTravel by lazy {
        Travel()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupMap()
        setupPlaces()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.menu_main_activity, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        return when (item.itemId) {
            R.id.add_travel_item -> {
                addTravelItem()
                true
            }
            R.id.open_travel_list -> {
                confirmOpenList()
                true
            }
            R.id.share -> {
                shareViaWhatsApp()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun shareViaWhatsApp() {
        val whatsappIntent = Intent(Intent.ACTION_SEND)
        whatsappIntent.type = "text/plain"
        whatsappIntent.setPackage("com.whatsapp")
        whatsappIntent.putExtra(
            Intent.EXTRA_TEXT,
            ModelController.get().getListAsString()
        )

        try {
            startActivity(whatsappIntent)
        } catch (ex: ActivityNotFoundException) {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=com.whatsapp")
                )
            )
        }
    }

    private fun confirmOpenList() {
        // Open directly as no calculation has been made
        if (currentTravel.from.isEmpty() || currentTravel.to.isEmpty()) {
            openTravelList()
            return
        }

        val title = getString(R.string.dialog_title_confirm_operation)

        showConfirmDialog(
            title,
            getString(R.string.dialog_message_open_list_without_saving),
            "Sí",
            actionIfAgree = {
                openTravelList()
            })
    }

    private fun addTravelItem() {
        Log.d(TAG, "addTravelItem")

        if (currentTravel.from.isEmpty()) {
            toast("Ingrese una dirección de origen")
            return
        }

        if (currentTravel.to.isEmpty()) {
            toast("Ingrese una dirección de destino")
            return
        }

        if (currentTravel.distance == -1) {
            toast("Aún no se ha calculado la distancia")
            return
        }

        val title = getString(R.string.dialog_title_confirm_operation)

        showConfirmDialog(
            title,
            "¿Está seguro que desea añadir este viaje a su lista?",
            "Sí",
            actionIfAgree = {
                ModelController.get().addTravel(currentTravel)
                openTravelList()
                toast("Agregado a la lista de viajes")
            })
    }

    private fun openTravelList() {
        val intent = Intent(this, TravelListActivity::class.java)
        startActivity(intent)
    }

    private fun setupMap() {
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment

        mapFragment.getMapAsync(this)
    }

    private fun setupPlaces() {
        Places.initialize(applicationContext, getString(R.string.android_sdk_places_api_key))

        btnFrom.setOnClickListener {
            startAutocomplete(REQUEST_CODE_AUTOCOMPLETE_FROM)
        }

        btnTo.setOnClickListener {
            startAutocomplete(REQUEST_CODE_AUTOCOMPLETE_TO)
        }

        tvFrom.text = getString(R.string.label_from, getString(R.string.no_place_selected))
        tvTo.text = getString(R.string.label_from, getString(R.string.no_place_selected))
    }

    private fun startAutocomplete(requestCode: Int) {
        // Fields of place data to return after the user has made a selection.
        val fields = listOf(
            Place.Field.ID,
            Place.Field.NAME,
            Place.Field.LAT_LNG,
            Place.Field.ADDRESS
        )

        // Start the autocomplete intent.
        val intent = Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields)
                .build(this)

        startActivityForResult(intent, requestCode)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_CODE_AUTOCOMPLETE_FROM) {
            processAutocompleteResult(resultCode, data) { place ->
                tvFrom.text = getString(R.string.label_from, place.address)

                currentTravel.from = place.address.toString()

                place.latLng?.let {
                    mFromLatLng = it
                    setMarkerFrom(it)
                }
            }
            return
        } else if (requestCode == REQUEST_CODE_AUTOCOMPLETE_TO) {
            processAutocompleteResult(resultCode, data) { place ->
                tvTo.text = getString(R.string.label_to, place.address)

                currentTravel.to = place.address.toString()

                place.latLng?.let {
                    mToLatLng = it
                    setMarkerTo(it)
                }
            }
            return
        }

        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun processAutocompleteResult(
        resultCode: Int, data: Intent?,
        callback: (Place) -> Unit
    ) {
        Log.d(TAG, "processAutocompleteResult(resultCode=$resultCode)")

        when (resultCode) {
            Activity.RESULT_OK -> {
                data?.let {
                    val place = Autocomplete.getPlaceFromIntent(data)
                    Log.i(TAG, "Place: $place")
                    callback(place)
                }
            }
            AutocompleteActivity.RESULT_ERROR -> {
                data?.let {
                    val status = Autocomplete.getStatusFromIntent(data)
                    status.statusMessage?.let { message ->
                        Log.i(TAG, message)
                    }
                }
            }
        }
    }

    /**
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera.
     *
     * In this case, we add 2 markers (origin and destination).
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        // Set zoom preferences
        mMap.setMinZoomPreference(12f)
        mMap.setMaxZoomPreference(14f)
    }

    private fun addMarker(latLng: LatLng, title: String): Marker {
        val markerOptions = MarkerOptions()
                .position(latLng)
                .title(title)

        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng))

        return mMap.addMarker(markerOptions)
    }

    private fun setMarkerFrom(latLng: LatLng) {
        // if already set, remove it
        mMarkerFrom?.remove()

        mMarkerFrom = addMarker(latLng, getString(R.string.marker_title_from))

        computeTravelInfo()
    }

    private fun setMarkerTo(latLng: LatLng) {
        // if already set, remove it
        mMarkerTo?.remove()

        mMarkerTo = addMarker(latLng, getString(R.string.marker_title_to))

        computeTravelInfo()
    }

    /**
     * We ask the server
     */
    private fun computeTravelInfo() {
        val from = mFromLatLng?.let { latLngToStr(it) }
        val to = mToLatLng?.let { latLngToStr(it) }

        if (from == null || to == null) {
            return
        }

        ApiService.create().getDistance(from, to).enqueue(object : Callback<DistanceResponse> {
            override fun onResponse(
                call: Call<DistanceResponse>,
                response: Response<DistanceResponse>
            ) {
                if (response.isSuccessful) {
                    response.body()?.let {
                        setTravelInfo(it)
                    }

                } else {
                    Log.d(TAG, "Unexpected response from the server")
                }
            }

            override fun onFailure(call: Call<DistanceResponse>, t: Throwable) {
                Log.d(TAG, t.localizedMessage ?: "")
            }

        })
    }

    private fun setTravelInfo(distanceResponse: DistanceResponse) {
        currentTravel.distance = distanceResponse.distance
        currentTravel.time = distanceResponse.time
        currentTravel.price = distanceResponse.price

        displayTravelInfo()
        drawRoute(distanceResponse.steps)
    }

    private fun displayTravelInfo() {
        tvDistance.text = getString(R.string.label_distance_km, currentTravel.getDistanceInKm())
        tvTime.text = getString(R.string.label_time, currentTravel.time)
        tvPrice.text = getString(R.string.label_price, currentTravel.getPriceRoundedAsText())

        llTravelInfo.visibility = View.VISIBLE
    }

    private var lastPolyline: Polyline? = null

    private fun drawRoute(steps: List<Step>?) {
        Log.d(TAG, "drawRoute")

        lastPolyline?.remove()

        if (steps==null || steps.isEmpty()) {
            Log.d(TAG, "Couldn't draw route because steps were not provided")
            return
        }

        val options = PolylineOptions().clickable(true)

        options.add(steps[0].start_location.toLatLng())

        steps.forEach {
            options.add(it.end_location.toLatLng())
        }

        // Add a polyline representing the route
        lastPolyline = mMap.addPolyline(options)
    }

    private fun latLngToStr(latLng: LatLng) = "${latLng.latitude},${latLng.longitude}"
}