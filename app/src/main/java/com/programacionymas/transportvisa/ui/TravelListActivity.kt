package com.programacionymas.transportvisa.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.programacionymas.transportvisa.R
import com.programacionymas.transportvisa.model.ModelController
import com.programacionymas.transportvisa.ui.adapter.TravelAdapter
import kotlinx.android.synthetic.main.activity_travel_list.*

class TravelListActivity : AppCompatActivity(), TravelAdapter.OnChangeListener {

    private val travelList by lazy {
        ModelController.get().getTravelList()
    }

    private val travelAdapter by lazy {
        TravelAdapter(travelList, this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_travel_list)

        setupRecyclerView()
        showTotal()
    }

    private fun setupRecyclerView() {
        val layoutManager = LinearLayoutManager(baseContext)
        rvTravelList.layoutManager = layoutManager
        rvTravelList.adapter = travelAdapter


    }

    private fun showTotal() {
        tvTotal.text = getString(R.string.label_total, ModelController.get().getTotal())
    }

    override fun onElementRemoved() {
        showTotal()
    }
}