package com.programacionymas.transportvisa.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.recyclerview.widget.RecyclerView
import com.programacionymas.transportvisa.R
import com.programacionymas.transportvisa.model.Travel
import kotlinx.android.synthetic.main.item_travel.view.*
import kotlinx.android.synthetic.main.layout_travel_info.view.*

class TravelAdapter (private val mDataSet: ArrayList<Travel>, private val onChangeListener: OnChangeListener)
    : RecyclerView.Adapter<TravelAdapter.ViewHolder>() {

    interface OnChangeListener {
        fun onElementRemoved();
    }

    class ViewHolder(itemView: View) :  RecyclerView.ViewHolder(itemView) {

        val btnDelete: Button = itemView.btnDeleteItem

        fun bind(travel: Travel, travelId: Int) = with (itemView) {
            tvTravelId.text = context.getString(R.string.label_travel_id, travelId)

            tvFrom.text = context.getString(R.string.label_from, travel.from)
            tvTo.text = context.getString(R.string.label_to, travel.to)

            tvDistance.text = context.getString(R.string.label_distance_km, travel.getDistanceInKm())
            tvTime.text = context.getString(R.string.label_time, travel.time)
            tvPrice.text = context.getString(R.string.label_price, travel.getPriceRoundedAsText())

            llTravelInfo.visibility = View.VISIBLE
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_travel, parent, false)

        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val travel = mDataSet[position]

        holder.bind(travel, position +1)

        holder.btnDelete.setOnClickListener {
            mDataSet.removeAt(position)
            notifyDataSetChanged()
            onChangeListener.onElementRemoved() // notify to the host activity
        }
    }

    override fun getItemCount() = mDataSet.size
}