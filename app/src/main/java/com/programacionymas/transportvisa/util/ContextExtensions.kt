package com.programacionymas.transportvisa.util

import android.app.AlertDialog
import android.content.Context
import android.widget.Toast

fun Context.toast(message: CharSequence) =
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()

fun Context.showConfirmDialog(
    title: String,
    message: String?,
    positiveBtn: String = "Ok", negativeBtn: String = "Cancel",
    actionIfAgree: (() -> Unit)? = null,
    actionIfDisagree: (() -> Unit)? = null
) {
    val alertDialog = AlertDialog.Builder(this).create()
    alertDialog.setTitle(title)
    alertDialog.setMessage(message)

    alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, negativeBtn) { dialog, _ ->
        actionIfDisagree?.let { it() }
        dialog.dismiss()
    }

    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, positiveBtn) { dialog, _ ->
        actionIfAgree?.let { it() }
        dialog.dismiss()
    }

    alertDialog.show()
}